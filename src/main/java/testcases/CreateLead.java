package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead extends Annotations{

	@BeforeTest (groups ="smoke")
	public void setdata()
	{
		testcaseName="CreateLead";
		testcaseDec="Creating a Lead";
		author="Maybesomeone";
		category="Smoke";
		excelname ="createlead";

	}
	
	//@Test(timeOut=10000)
	//@Test (invocationCount=2,invocationTimeOut=20000)
	/*@Test (groups ="smoke",dependsOnGroups="sanity")*/
	@Test(groups="smoke",dataProvider="createData")


	public void Create_Lead(String cname, String fName, String lname) throws InterruptedException {
		WebElement eleCrmSfa = locateElement("link", "CRM/SFA");
		click(eleCrmSfa);
		WebElement elecreatelead = locateElement("link", "Create Lead");
		click(elecreatelead);
		WebElement elecompany = locateElement("id", "createLeadForm_companyName");
		clearAndType(elecompany, cname);
		WebElement eleFNAME = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFNAME, fName);
		WebElement eleLNAME = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLNAME, lname);
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Conference");
		WebElement createLeadBtn = locateElement("xpath", "//input[@class='smallSubmit']");
		createLeadBtn.click();
		Thread.sleep(3000);
		/*WebElement eleCrmSfa = locateElement("link", "CRM/SFA");
click(eleCrmSfa);
WebElement elecreatelead = locateElement("link", "Create Lead");
click(elecreatelead);
WebElement elecompany = locateElement("id", "createLeadForm_companyName");
WebElement eleFNAME = locateElement("id", "createLeadForm_firstName");
clearAndType(eleFNAME, "Fname");
WebElement eleLNAME = locateElement("id", "createLeadForm_lastName");
clearAndType(eleLNAME, "Lname");
WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
selectDropDownUsingText(eleSource, "Conference");
WebElement createLeadBtn = locateElement("xpath", "//input[@class='smallSubmit']");
createLeadBtn.click();
Thread.sleep(3000);*/

	}





}
