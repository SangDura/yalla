package testcases;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class AcmeTest {

/*	public static void main(String[] args) throws Exception {*/
	@Test
	public void project() throws Exception {
	
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@id='email']").sendKeys("hemanathanmohan@gmail.com");
		driver.findElementByXPath("//input[@id='password']").sendKeys("hemanathan123");
		driver.findElementById("buttonLogin").click();
		Thread.sleep(3000);
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElementByXPath("//button[text()=' Vendors']")).build().perform();
		driver.findElementByXPath("//a[text()='Search for Vendor']").click();
		driver.findElementByXPath("(//*[contains(text(),' Vendor')])[1]");
		driver.findElementById("vendorTaxID").sendKeys("DE987564");
		driver.findElementByXPath("//button[@id='buttonSearch']").click();
		String finaltext = driver.findElementByXPath("(//table[@class='table']/tbody/tr//following::tr/td)[1]").getText();
	
		System.out.println(finaltext);
		driver.close();
	}}