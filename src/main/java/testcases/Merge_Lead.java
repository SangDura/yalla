package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

	public class Merge_Lead extends Annotations{
		@BeforeTest (groups ="sanity")
		public void setdata()
		{
			testcaseName="CreateLead";
			testcaseDec="Creating a Lead";
			author="nothema";
			category="sanity";
			
		}
		
		@Test (groups ="sanity")
	public void MergeLead() throws Throwable {
			WebElement eleCrmSfa = locateElement("link", "CRM/SFA");
			click(eleCrmSfa);
			WebElement elecreatelead = locateElement("link", "Create Lead");
			click(elecreatelead);
			WebElement eleMergelead = locateElement("link", "Merge Leads");
			click(eleMergelead);
			System.out.println(driver.getTitle());
			WebElement eleFrom = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
			click(eleFrom);
			/*Set<String> allwins= driver.getWindowHandles();
			List<String> lst= new ArrayList<>();
			lst.addAll(allwins);
			driver.switchTo().window(lst.get(1));
			WebElement eleLeadList = locateElement("xpath", "(//a[@class='linktext'])[1]");
			click(eleLeadList);
			driver.switchTo().window(lst.get(0));
			WebElement eleTo = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
			click(eleTo);
			driver.switchTo().window(lst.get(1));
			Thread.sleep(2000);
			WebElement eleLeadList2 = locateElement("xpath", "(//a[@class='linktext'])[1]");
			click(eleLeadList2);
		//	driver.switchTo().window(lst.get(0));
			driver.switchTo().defaultContent();
			WebElement eleMerge = locateElement("class", " ext-safari");
			click(eleMerge);*/
			
}
}