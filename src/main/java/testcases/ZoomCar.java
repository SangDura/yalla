package testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

	public class ZoomCar extends Annotations{
		
		@BeforeTest
		
		public void setdata() {
			testcaseDec="book the car";
			testcaseName="tc001_zoomcar";
			author="Sang";
			category="REGESSION";
		}
		
		
		
		
		@Test
	public void BookCar() throws Throwable {
			
			WebElement start = locateElement("xpath", "//a[@title='Start your wonderful journey']");
			click(start);
			WebElement Location  =locateElement("xpath", "(//div[@class='items'])[1]");
			click(Location);
			WebElement next = locateElement("class", "proceed");
			click(next);
			WebElement day = locateElement("xpath", "(//div[@class='day low-price'])[1]");
			click(day);
			// Get the current date
			
			Date date = new Date();

			 //Get only the date (and not month, year, time etc)
					
			DateFormat sdf = new SimpleDateFormat("6");
			 
			// Get today's date
					
			String today = sdf.format(date);

			// Convert to integer and add 1 to it
			 
			int tomorrow = Integer.parseInt(today)+1;

			// Print tomorrow's date
					
			System.out.println(tomorrow);
			
			WebElement next1 = locateElement("class", "proceed");
			click(next1);
			WebElement done = locateElement("class", "proceed");
			click(done);
			List<WebElement> car= locateElements("xpath", "//div[@class='price']");
			List<Integer> p = new ArrayList<>();
			
			
			for (WebElement eachCar : car) {
				p.add(Integer.parseInt(eachCar.getText().replaceAll("\\D", "")));
			//	System.out.println(Integer.parseInt(eachCar.getText().replaceAll("\\D", "")));
			}
			
			System.out.println(Collections.max(p));
			
			int max = Collections.max(p);
			
			WebElement bName = locateElement("xpath", "//div[contains(text(),'"+max+"')]/preceding::h3[1]");
			
			
			WebElement price = locateElement("xpath", "//div[contains(text(),'"+max+"')]");
			
			
			System.out.println(bName.getText()+"------->"+price.getText());
			
			WebElement bookNow = locateElement("xpath", "//div[contains(text(),'\"+max+\"')]/following::button[1]");
			click(bookNow);
			driver.close();
}
}